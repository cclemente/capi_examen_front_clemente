import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablausuariosComponent } from './tablausuarios/tablausuarios.component';



@NgModule({
  declarations: [
    TablausuariosComponent
  ],
  imports: [
    CommonModule
  ]
})
export class UsuariosModule { }
