import { Component, OnInit ,AfterViewInit, ViewChild} from '@angular/core';
import { JsonService } from 'src/app/json.service';


@Component({
  selector: 'app-tablausuarios',
  templateUrl: './tablausuarios.component.html',
  styleUrls: ['./tablausuarios.component.css']
})
export class TablausuariosComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];

  url ='http://localhost:8001/api/domicilios';
  items=[];

  title = 'capi_examen_front_Clemente';

  constructor(public json: JsonService){
    
    this.json.getJson(this.url).subscribe((res:any)=>{
      console.log(res);
      this.items=res
    })
    
  }
  
  ngOnInit(): void {
  }
}
