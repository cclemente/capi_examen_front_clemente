import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { JsonService } from './json.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  url ='http://localhost:8001/api/domicilios';
  items=[];

  title = 'capi_examen_front_Clemente';

  constructor(public json: JsonService){
    this.json.getJson(this.url).subscribe((res:any)=>{
      console.log(res);
      this.items=res
    })
  }
}
