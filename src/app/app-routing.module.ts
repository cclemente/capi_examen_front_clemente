import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TablausuariosComponent } from './usuarios/tablausuarios/tablausuarios.component';
const routes: Routes = [
  {path:'',component:TablausuariosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [TablausuariosComponent]